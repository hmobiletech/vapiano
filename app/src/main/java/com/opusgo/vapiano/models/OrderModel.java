package com.opusgo.vapiano.models;

import java.util.ArrayList;

/**
 * Created by redugsi on 22/03/17.
 */

public class OrderModel {
    private long OrderID;
    private long WorkStationID;
    private long EmployeeID;
    private String CardNo;
    private String OrderNote;
    private String CreatedDateTime;
    private ArrayList<OrderProductModel> OrderProducts;

    public OrderModel() {

    }

    public OrderModel(long orderID, long workStationID, long employeeID, String cardNo, String orderNote, String createdDateTime, ArrayList<OrderProductModel> orderProducts) {
        OrderID = orderID;
        WorkStationID = workStationID;
        EmployeeID = employeeID;
        CardNo = cardNo;
        OrderNote = orderNote;
        CreatedDateTime = createdDateTime;
        OrderProducts = orderProducts;
    }

    public long getOrderID() {
        return OrderID;
    }

    public void setOrderID(long orderID) {
        OrderID = orderID;
    }

    public long getWorkStationID() {
        return WorkStationID;
    }

    public void setWorkStationID(long workStationID) {
        WorkStationID = workStationID;
    }

    public long getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(long employeeID) {
        EmployeeID = employeeID;
    }

    public String getCardNo() {
        return CardNo;
    }

    public void setCardNo(String cardNo) {
        CardNo = cardNo;
    }

    public String getOrderNote() {
        return OrderNote;
    }

    public void setOrderNote(String orderNote) {
        OrderNote = orderNote;
    }

    public String getCreatedDateTime() {
        return CreatedDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        CreatedDateTime = createdDateTime;
    }

    public ArrayList<OrderProductModel> getOrderProducts() {
        return OrderProducts;
    }

    public void setOrderProducts(ArrayList<OrderProductModel> orderProducts) {
        OrderProducts = orderProducts;
    }
}
