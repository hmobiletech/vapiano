package com.opusgo.vapiano.models;

/**
 * Created by hakankanmaz on 22.03.2017.
 */

public class AuthorizedMembersRequest {

    String DeviceCode;
    long WorkStationID;

    public String getDeviceCode() {
        return DeviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        DeviceCode = deviceCode;
    }

    public long getWorkStationID() {
        return WorkStationID;
    }

    public void setWorkStationID(long workStationID) {
        WorkStationID = workStationID;
    }
}
