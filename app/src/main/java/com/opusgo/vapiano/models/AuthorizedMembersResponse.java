package com.opusgo.vapiano.models;

/**
 * Created by hakankanmaz on 22.03.2017.
 */

public class AuthorizedMembersResponse {


    long MemberID;
    String FullName;
    String PhotoImageUrl;
    String MemberCode;
    String Pass;


    public AuthorizedMembersResponse(long memberID, String fullName, String photoImageUrl, String memberCode, String pass) {
        MemberID = memberID;
        FullName = fullName;
        PhotoImageUrl = photoImageUrl;
        MemberCode = memberCode;
        Pass = pass;
    }

    public AuthorizedMembersResponse() {
    }

    public long getMemberID() {
        return MemberID;
    }

    public void setMemberID(long memberID) {
        MemberID = memberID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getPhotoImageUrl() {
        return PhotoImageUrl;
    }

    public void setPhotoImageUrl(String photoImageUrl) {
        PhotoImageUrl = photoImageUrl;
    }

    public String getMemberCode() {
        return MemberCode;
    }

    public void setMemberCode(String memberCode) {
        MemberCode = memberCode;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String pass) {
        Pass = pass;
    }
}
