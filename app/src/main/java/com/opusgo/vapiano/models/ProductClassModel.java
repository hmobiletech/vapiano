package com.opusgo.vapiano.models;

/**
 * Created by redugsi on 21/02/17.
 */

public class ProductClassModel {

    private int ProductClassID;
    private String ClassName;

    public ProductClassModel(int productClassID, String className) {
        ProductClassID = productClassID;
        ClassName = className;
    }

    public ProductClassModel(){}

    public int getProductClassID() {
        return ProductClassID;
    }

    public void setProductClassID(int productClassID) {
        ProductClassID = productClassID;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }
}
