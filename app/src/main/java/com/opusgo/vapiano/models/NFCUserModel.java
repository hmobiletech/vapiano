package com.opusgo.vapiano.models;

/**
 * Created by redugsi on 20/02/17.
 */

public class NFCUserModel {

    private String cardNumber;
    private String companyNo;
    private String limit;
    private String customerName;
    private String customerSurname;
    private String createdDate;
    private String cartType;

    public NFCUserModel(String cardNumber, String companyNo, String limit, String customerName, String customerSurname, String createdDate, String cartType) {
        this.cardNumber = cardNumber;
        this.companyNo = companyNo;
        this.limit = limit;
        this.customerName = customerName;
        this.customerSurname = customerSurname;
        this.createdDate = createdDate;
        this.cartType = cartType;
    }

    public NFCUserModel() {

    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCompanyNo() {
        return companyNo;
    }

    public void setCompanyNo(String companyNo) {
        this.companyNo = companyNo;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerSurname() {
        return customerSurname;
    }

    public void setCustomerSurname(String customerSurname) {
        this.customerSurname = customerSurname;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCartType() {
        return cartType;
    }

    public void setCartType(String cartType) {
        this.cartType = cartType;
    }

    @Override
    public String toString() {
        return "NFCUserModel{" +
                "cardNumber='" + cardNumber + '\'' +
                ", companyNo='" + companyNo + '\'' +
                ", limit='" + limit + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerSurname='" + customerSurname + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", cartType='" + cartType + '\'' +
                '}';
    }
}
