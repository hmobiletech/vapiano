package com.opusgo.vapiano.models;

import java.util.ArrayList;

/**
 * Created by hakankanmaz on 5.02.2017.
 */

public class ProductModel implements Comparable<ProductModel>{

    long ProductID;
    int ProductTypeID;
    int ProductBrandID;
    int PurchasingTypeID;
    int CompanyID;
    int WarehouseLocationID;
    String Name;
    String ProductCode;
    String ProductSpeacialCode;
    String Barcode;
    String ManufacturerPartCode;
    double StandartCost;
    int StandartCostCurrencyID;
    double ListPrice;
    int ListPriceCurrencyID;
    int ReOrderPoint;
    int ProductClassID;
    int SafetyStockLevel;
    String SellStartDate;
    String SellEndDate;
    String MakeFlag;
    String ProductTags;
    String CustomTariffCode;
    String CatalogCode;
    String ShortDescription;
    String LongDescription;
    double PurchaseTaxRate;
    double SalesTaxRate;
    boolean IncludeTaxForPurchasing;
    boolean IncludeTaxForSales;
    double MinProfitMargin;
    double MaxProfitMargin;
    double MinSalesQuantity;
    int MeasureUnitID;
    String IncludeOTV;
    String ShelfLife;
    int ShelfLifeMeasureID;
    int PackingControlTypeID;
    double ProductWeight;
    String ProductDimensions;
    double MinOrderQuantity;
    double ProfitRate;
    boolean IsAutoCalculateListPrice;
    int DisplayOrder;
    String Created;
    String ModifiedDate;
    int StatusTypeID;
    ArrayList<String> OrderProducts;
    ArrayList<String>  ProductCategoryItems;
    String ProductTypes;
    boolean isHeader;
    long orderProductId;

    public long getOrderProductId() {
        return orderProductId;
    }

    public void setOrderProductId(long orderProductId) {
        this.orderProductId = orderProductId;
    }

    public long getProductID() {
        return ProductID;
    }

    public void setProductID(long productID) {
        ProductID = productID;
    }

    public int getProductTypeID() {
        return ProductTypeID;
    }

    public void setProductTypeID(int productTypeID) {
        ProductTypeID = productTypeID;
    }

    public int getProductBrandID() {
        return ProductBrandID;
    }

    public void setProductBrandID(int productBrandID) {
        ProductBrandID = productBrandID;
    }

    public int getPurchasingTypeID() {
        return PurchasingTypeID;
    }

    public void setPurchasingTypeID(int purchasingTypeID) {
        PurchasingTypeID = purchasingTypeID;
    }

    public int getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(int companyID) {
        CompanyID = companyID;
    }

    public int getWarehouseLocationID() {
        return WarehouseLocationID;
    }

    public void setWarehouseLocationID(int warehouseLocationID) {
        WarehouseLocationID = warehouseLocationID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getProductSpeacialCode() {
        return ProductSpeacialCode;
    }

    public void setProductSpeacialCode(String productSpeacialCode) {
        ProductSpeacialCode = productSpeacialCode;
    }

    public String getBarcode() {
        return Barcode;
    }

    public void setBarcode(String barcode) {
        Barcode = barcode;
    }

    public String getManufacturerPartCode() {
        return ManufacturerPartCode;
    }

    public void setManufacturerPartCode(String manufacturerPartCode) {
        ManufacturerPartCode = manufacturerPartCode;
    }

    public double getStandartCost() {
        return StandartCost;
    }

    public void setStandartCost(double standartCost) {
        StandartCost = standartCost;
    }

    public int getStandartCostCurrencyID() {
        return StandartCostCurrencyID;
    }

    public void setStandartCostCurrencyID(int standartCostCurrencyID) {
        StandartCostCurrencyID = standartCostCurrencyID;
    }

    public double getListPrice() {
        return ListPrice;
    }

    public void setListPrice(double listPrice) {
        ListPrice = listPrice;
    }

    public int getListPriceCurrencyID() {
        return ListPriceCurrencyID;
    }

    public void setListPriceCurrencyID(int listPriceCurrencyID) {
        ListPriceCurrencyID = listPriceCurrencyID;
    }

    public int getReOrderPoint() {
        return ReOrderPoint;
    }

    public void setReOrderPoint(int reOrderPoint) {
        ReOrderPoint = reOrderPoint;
    }

    public int getProductClassID() {
        return ProductClassID;
    }

    public void setProductClassID(int productClassID) {
        ProductClassID = productClassID;
    }

    public int getSafetyStockLevel() {
        return SafetyStockLevel;
    }

    public void setSafetyStockLevel(int safetyStockLevel) {
        SafetyStockLevel = safetyStockLevel;
    }

    public String getSellStartDate() {
        return SellStartDate;
    }

    public void setSellStartDate(String sellStartDate) {
        SellStartDate = sellStartDate;
    }

    public String getSellEndDate() {
        return SellEndDate;
    }

    public void setSellEndDate(String sellEndDate) {
        SellEndDate = sellEndDate;
    }

    public String getMakeFlag() {
        return MakeFlag;
    }

    public void setMakeFlag(String makeFlag) {
        MakeFlag = makeFlag;
    }

    public String getProductTags() {
        return ProductTags;
    }

    public void setProductTags(String productTags) {
        ProductTags = productTags;
    }

    public String getCustomTariffCode() {
        return CustomTariffCode;
    }

    public void setCustomTariffCode(String customTariffCode) {
        CustomTariffCode = customTariffCode;
    }

    public String getCatalogCode() {
        return CatalogCode;
    }

    public void setCatalogCode(String catalogCode) {
        CatalogCode = catalogCode;
    }

    public String getShortDescription() {
        return ShortDescription;
    }

    public void setShortDescription(String shortDescription) {
        ShortDescription = shortDescription;
    }

    public String getLongDescription() {
        return LongDescription;
    }

    public void setLongDescription(String longDescription) {
        LongDescription = longDescription;
    }

    public double getPurchaseTaxRate() {
        return PurchaseTaxRate;
    }

    public void setPurchaseTaxRate(double purchaseTaxRate) {
        PurchaseTaxRate = purchaseTaxRate;
    }

    public double getSalesTaxRate() {
        return SalesTaxRate;
    }

    public void setSalesTaxRate(double salesTaxRate) {
        SalesTaxRate = salesTaxRate;
    }

    public boolean isIncludeTaxForPurchasing() {
        return IncludeTaxForPurchasing;
    }

    public void setIncludeTaxForPurchasing(boolean includeTaxForPurchasing) {
        IncludeTaxForPurchasing = includeTaxForPurchasing;
    }

    public boolean isIncludeTaxForSales() {
        return IncludeTaxForSales;
    }

    public void setIncludeTaxForSales(boolean includeTaxForSales) {
        IncludeTaxForSales = includeTaxForSales;
    }

    public double getMinProfitMargin() {
        return MinProfitMargin;
    }

    public void setMinProfitMargin(double minProfitMargin) {
        MinProfitMargin = minProfitMargin;
    }

    public double getMaxProfitMargin() {
        return MaxProfitMargin;
    }

    public void setMaxProfitMargin(double maxProfitMargin) {
        MaxProfitMargin = maxProfitMargin;
    }

    public double getMinSalesQuantity() {
        return MinSalesQuantity;
    }

    public void setMinSalesQuantity(double minSalesQuantity) {
        MinSalesQuantity = minSalesQuantity;
    }

    public int getMeasureUnitID() {
        return MeasureUnitID;
    }

    public void setMeasureUnitID(int measureUnitID) {
        MeasureUnitID = measureUnitID;
    }

    public String getIncludeOTV() {
        return IncludeOTV;
    }

    public void setIncludeOTV(String includeOTV) {
        IncludeOTV = includeOTV;
    }

    public String getShelfLife() {
        return ShelfLife;
    }

    public void setShelfLife(String shelfLife) {
        ShelfLife = shelfLife;
    }

    public int getShelfLifeMeasureID() {
        return ShelfLifeMeasureID;
    }

    public void setShelfLifeMeasureID(int shelfLifeMeasureID) {
        ShelfLifeMeasureID = shelfLifeMeasureID;
    }

    public int getPackingControlTypeID() {
        return PackingControlTypeID;
    }

    public void setPackingControlTypeID(int packingControlTypeID) {
        PackingControlTypeID = packingControlTypeID;
    }

    public double getProductWeight() {
        return ProductWeight;
    }

    public void setProductWeight(double productWeight) {
        ProductWeight = productWeight;
    }

    public String getProductDimensions() {
        return ProductDimensions;
    }

    public void setProductDimensions(String productDimensions) {
        ProductDimensions = productDimensions;
    }

    public double getMinOrderQuantity() {
        return MinOrderQuantity;
    }

    public void setMinOrderQuantity(double minOrderQuantity) {
        MinOrderQuantity = minOrderQuantity;
    }

    public double getProfitRate() {
        return ProfitRate;
    }

    public void setProfitRate(double profitRate) {
        ProfitRate = profitRate;
    }

    public boolean isAutoCalculateListPrice() {
        return IsAutoCalculateListPrice;
    }

    public void setAutoCalculateListPrice(boolean autoCalculateListPrice) {
        IsAutoCalculateListPrice = autoCalculateListPrice;
    }

    public int getDisplayOrder() {
        return DisplayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        DisplayOrder = displayOrder;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public int getStatusTypeID() {
        return StatusTypeID;
    }

    public void setStatusTypeID(int statusTypeID) {
        StatusTypeID = statusTypeID;
    }

    public ArrayList<String> getOrderProducts() {
        return OrderProducts;
    }

    public void setOrderProducts(ArrayList<String> orderProducts) {
        OrderProducts = orderProducts;
    }

    public ArrayList<String> getProductCategoryItems() {
        return ProductCategoryItems;
    }

    public void setProductCategoryItems(ArrayList<String> productCategoryItems) {
        ProductCategoryItems = productCategoryItems;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public String getProductTypes() {
        return ProductTypes;
    }

    public void setProductTypes(String productTypes) {
        ProductTypes = productTypes;
    }

    @Override
    public int compareTo(ProductModel productModel) {
        if(ProductClassID == productModel.getProductClassID()){
            return 0;
        }else if(ProductClassID < productModel.getProductClassID()){
            return -1;
        }else{
            return 1;
        }
    }
}
