package com.opusgo.vapiano.models;

import java.util.ArrayList;

/**
 * Created by hakankanmaz on 5.02.2017.
 */

public class CategoryModel {

    int CategoryID;
    String ProductTypeID;
    String Code;
    String Name;
    String Description;
    String ParentCategoryID;
    int Order;
    String Created;
    String ModifiedDate;
    int StatusTypeID;
    int CompanyID;
    String BackgroundColor;
    String FontColor;
    String BackgroundImage;
    String MenuImage;
    boolean IsPrint;
    boolean IsSales;
    boolean AllowDiscount;
    String ProductTypes;
    ArrayList<String> ProductCategoryItems;

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int categoryID) {
        CategoryID = categoryID;
    }

    public String getProductTypeID() {
        return ProductTypeID;
    }

    public void setProductTypeID(String productTypeID) {
        ProductTypeID = productTypeID;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getParentCategoryID() {
        return ParentCategoryID;
    }

    public void setParentCategoryID(String parentCategoryID) {
        ParentCategoryID = parentCategoryID;
    }

    public int getOrder() {
        return Order;
    }

    public void setOrder(int order) {
        Order = order;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public int getStatusTypeID() {
        return StatusTypeID;
    }

    public void setStatusTypeID(int statusTypeID) {
        StatusTypeID = statusTypeID;
    }

    public int getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(int companyID) {
        CompanyID = companyID;
    }

    public String getBackgroundColor() {
        return BackgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        BackgroundColor = backgroundColor;
    }

    public String getFontColor() {
        return FontColor;
    }

    public void setFontColor(String fontColor) {
        FontColor = fontColor;
    }

    public String getBackgroundImage() {
        return BackgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        BackgroundImage = backgroundImage;
    }

    public String getMenuImage() {
        return MenuImage;
    }

    public void setMenuImage(String menuImage) {
        MenuImage = menuImage;
    }

    public boolean isPrint() {
        return IsPrint;
    }

    public void setPrint(boolean print) {
        IsPrint = print;
    }

    public boolean isSales() {
        return IsSales;
    }

    public void setSales(boolean sales) {
        IsSales = sales;
    }

    public boolean isAllowDiscount() {
        return AllowDiscount;
    }

    public void setAllowDiscount(boolean allowDiscount) {
        AllowDiscount = allowDiscount;
    }

    public String getProductTypes() {
        return ProductTypes;
    }

    public void setProductTypes(String productTypes) {
        ProductTypes = productTypes;
    }

    public ArrayList<String> getProductCategoryItems() {
        return ProductCategoryItems;
    }

    public void setProductCategoryItems(ArrayList<String> productCategoryItems) {
        ProductCategoryItems = productCategoryItems;
    }
}
