package com.opusgo.vapiano.models;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by redugsi on 22/03/17.
 */

public class OrderProductModel {

    private long OrderProductID;
    private long ProductID;
    private double ProductQuantity;
    private String ProductName;
    private double ProductPrice;
    private String ProductNote;
    private String CreatedDateTime;

    public OrderProductModel() {

    }

    public OrderProductModel(long orderProductID, long productID, double productQuantity, String productName, double productPrice, String productNote, String createdDateTime) {
        OrderProductID = orderProductID;
        ProductID = productID;
        ProductQuantity = productQuantity;
        ProductName = productName;
        ProductPrice = productPrice;
        ProductNote = productNote;
        CreatedDateTime = createdDateTime;
    }

    public long getOrderProductID() {
        return OrderProductID;
    }

    public void setOrderProductID(long orderProductID) {
        OrderProductID = orderProductID;
    }

    public long getProductID() {
        return ProductID;
    }

    public void setProductID(long productID) {
        ProductID = productID;
    }

    public double getProductQuantity() {
        return ProductQuantity;
    }

    public void setProductQuantity(double productQuantity) {
        ProductQuantity = productQuantity;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public double getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(double productPrice) {
        ProductPrice = productPrice;
    }

    public String getProductNote() {
        return ProductNote;
    }

    public void setProductNote(String productNote) {
        ProductNote = productNote;
    }

    public String getCreatedDateTime() {
        return CreatedDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        CreatedDateTime = createdDateTime;
    }
}
