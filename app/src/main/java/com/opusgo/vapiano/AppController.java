package com.opusgo.vapiano;

import android.app.Activity;
import android.app.Application;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.opusgo.vapiano.models.AuthorizedMembersResponse;
import com.opusgo.vapiano.models.ProductClassListModel;

import java.util.ArrayList;

/**
 * Created by hakankanmaz on 14.02.2017.
 */

public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private AuthorizedMembersResponse waiter;
    private ArrayList<AuthorizedMembersResponse> personelList=new ArrayList<>();
    private ProductClassListModel productClasses;

    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public AuthorizedMembersResponse getWaiter() {
        return waiter;
    }

    public ArrayList<AuthorizedMembersResponse> getPersonelList(){

        return personelList;
    }

    public ProductClassListModel getProductClasses() {
        return productClasses;
    }

    public void setProductClasses(ProductClassListModel productClasses) {
        this.productClasses = productClasses;
    }

    public void setWaiter(AuthorizedMembersResponse waiter) {
        this.waiter = waiter;
    }

    public void setPersonelList(ArrayList<AuthorizedMembersResponse> personelList){
        this.personelList=personelList;
    }

    public static void setFullScreen(Activity activity) {
        WindowManager.LayoutParams attributes = activity.getWindow().getAttributes();
        attributes.flags |= WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
        activity.getWindow().setAttributes(attributes);

        activity.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }


    public static void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

}
