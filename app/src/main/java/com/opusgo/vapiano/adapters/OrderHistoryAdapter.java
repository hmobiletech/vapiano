package com.opusgo.vapiano.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.opusgo.vapiano.R;
import com.opusgo.vapiano.models.OrderModel;
import com.opusgo.vapiano.models.OrderProductModel;
import com.opusgo.vapiano.models.ProductModel;

import java.util.ArrayList;

/**
 * Created by erdemisguder on 3/24/17.
 */

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.MyViewHolder>{

    private ArrayList<OrderProductModel> orders;


    public OrderHistoryAdapter(ArrayList<OrderProductModel> orders){
        this.orders = orders;
        if (this.orders == null){
            this.orders = new ArrayList<>();
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_order_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final OrderProductModel order = orders.get(position);

        holder.lblProductName.setText(order.getProductName());
        holder.lblNumber.setText("1");
        holder.lblPrice.setText(String.valueOf(order.getProductPrice()));
        holder.lblAmount.setText(String.valueOf(order.getProductQuantity()));


//        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                removePriceListener.onRemove(orderListModelList.get(position).getListPrice());
//                removeAt(position);
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        if (orders == null){
            return 0;
        }

        return orders.size();
    }

    public void clearItems(){
        if (orders != null){
            orders.clear();
        }
    }

    public void setOrders(ArrayList<OrderProductModel> orderss){
        orders = orderss;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView lblProductName;
        public TextView lblNumber;
        public TextView lblPrice;
        public TextView lblAmount;
        public ImageButton btnDelete;


        public MyViewHolder(View view) {
            super(view);
            lblProductName = (TextView) view.findViewById(R.id.lblProductName);
            lblNumber = (TextView) view.findViewById(R.id.lblNumber);
            lblPrice = (TextView) view.findViewById(R.id.lblPrice);
            lblAmount = (TextView) view.findViewById(R.id.lblAmount);
            btnDelete = (ImageButton) view.findViewById(R.id.btnDelete);
        }
    }
}
