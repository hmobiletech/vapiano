package com.opusgo.vapiano.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.opusgo.vapiano.R;
import com.opusgo.vapiano.listeners.RemovePriceListener;
import com.opusgo.vapiano.models.CategoryModel;
import com.opusgo.vapiano.models.OrderListModel;
import com.opusgo.vapiano.models.ProductModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hakankanmaz on 5.02.2017.
 */

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.MyViewHolder> {

    private ArrayList<ProductModel> orderListModelList;
    RemovePriceListener removePriceListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView lblProductName;
        public TextView lblNumber;
        public TextView lblPrice;
        public TextView lblAmount;
        public ImageButton btnDelete;


        public MyViewHolder(View view) {
            super(view);
            lblProductName = (TextView) view.findViewById(R.id.lblProductName);
            lblNumber = (TextView) view.findViewById(R.id.lblNumber);
            lblPrice = (TextView) view.findViewById(R.id.lblPrice);
            lblAmount = (TextView) view.findViewById(R.id.lblAmount);
            btnDelete = (ImageButton) view.findViewById(R.id.btnDelete);

        }
    }


    public OrderListAdapter(ArrayList<ProductModel> orderListModelList, RemovePriceListener removePriceListener) {
        this.removePriceListener=removePriceListener;
        this.orderListModelList = orderListModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_order_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ProductModel orderListModel = orderListModelList.get(position);

        holder.lblProductName.setText(orderListModel.getName());
        holder.lblNumber.setText("1");
        holder.lblPrice.setText(String.valueOf(orderListModel.getListPrice()));
        holder.lblAmount.setText(String.valueOf(orderListModel.getListPrice()));


        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePriceListener.onRemove(orderListModelList.get(position).getListPrice());
                removeAt(position);

            }
        });

    }

    @Override
    public int getItemCount() {
        return orderListModelList.size();
    }

    public void removeAt(int position) {
        orderListModelList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, orderListModelList.size());
    }

    public void addItem(ProductModel item) {
        orderListModelList.add(item);
    }

    public void clearItems() {
        orderListModelList.clear();
    }

    public ArrayList<ProductModel> getAllItems(){
        return orderListModelList;
    }
}