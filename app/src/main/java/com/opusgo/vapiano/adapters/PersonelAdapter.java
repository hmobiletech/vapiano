package com.opusgo.vapiano.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.opusgo.vapiano.R;
import com.opusgo.vapiano.api.ApiUtil;
import com.opusgo.vapiano.models.AuthorizedMembersResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by hakankanmaz on 18/11/15.
 */
public class PersonelAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<AuthorizedMembersResponse> personelModelList;
    private Context mContext;


    public PersonelAdapter(Activity activity, List<AuthorizedMembersResponse> personelModelList, Context c) {
        mInflater = (LayoutInflater) activity.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        this.personelModelList = personelModelList;
        mContext = c;

    }

    @Override
    public int getCount() {
        return personelModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return personelModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView;
        rowView = mInflater.inflate(R.layout.row_personel, null);


        CircleImageView imgPersonel = (CircleImageView) rowView.findViewById(R.id.imgPersonel);
        TextView lblPersonelName = (TextView) rowView.findViewById(R.id.lblPersonelName);

        AuthorizedMembersResponse personelModel = personelModelList.get(position);

        lblPersonelName.setText(personelModel.getMemberCode());

        Picasso.with(mContext)
                .load(ApiUtil.BASE_IMAGE_URL+personelModel.getPhotoImageUrl())
                .placeholder(R.drawable.no_avatar)
                .error(R.drawable.no_avatar)
                .into(imgPersonel);

       /* String imgUrl="http://divapos.opusgo.com"+personelModel.getPhotoImageUrl();
        Glide.with(mContext).load("http://divapos.opusgo.com"+)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgPersonel);*/

      /*  String detail="";

        try {
            detail = new String(newsModel.getDetail().getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
        }
 */


        return rowView;
    }

}
