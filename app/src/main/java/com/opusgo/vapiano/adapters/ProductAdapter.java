package com.opusgo.vapiano.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.opusgo.vapiano.AppController;
import com.opusgo.vapiano.R;
import com.opusgo.vapiano.models.CategoryModel;
import com.opusgo.vapiano.models.ProductClassModel;
import com.opusgo.vapiano.models.ProductModel;
import com.opusgo.vapiano.models.ProductModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import dev.dworks.libs.astickyheader.SectionedGridAdapter;
import dev.dworks.libs.astickyheader.SimpleSectionedGridAdapter;

/**
 * Created by hakankanmaz on 18/11/15.
 */
public class ProductAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<ProductModel> productModelList;
    private List<ProductClassModel> classModels;
    private Context mContext;
    private int otherSectionCount;
    private SimpleSectionedGridAdapter gridAdapter;
    private static int position;

    public ProductAdapter(Activity activity, List<ProductModel> productModelList, Context c) {
        mInflater = (LayoutInflater) activity.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        this.productModelList = productModelList;
        mContext = c;
        classModels = new ArrayList<>();
        findSections();
    }

    @Override
    public int getCount() {
        return productModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return productModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        private TextView nameTextView;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;

        if (view == null) {
            view = mInflater.inflate(R.layout.row_product, null);
            holder = new ViewHolder();
            holder.nameTextView = (TextView) view.findViewById(R.id.lblProductName);
            view.setTag(holder);
        }else{
            holder = (ViewHolder)view.getTag();
        }


        ProductModel productModel = productModelList.get(position);
        holder.nameTextView.setText(productModel.getName());

      /*  Glide.with(mContext).load(R.drawable.no_avatar)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgPersonel);*/

      /*  String detail="";

        try {
            detail = new String(newsModel.getDetail().getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
        }
 */
        return view;
    }

    public void addItem(ProductModel item) {
        productModelList.add(item);
    }

    public void clearItems() {
        productModelList.clear();
        otherSectionCount = 0;
        classModels.clear();
    }


    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        findSections();
    }

    private void findSections() {
        if (gridAdapter == null){
            return;
        }
        Collections.sort(productModelList);
        if (AppController.getInstance().getProductClasses() != null) {
            for (int i = 0; i < AppController.getInstance().getProductClasses().size(); i++) {
                for (int j = 0; j < productModelList.size(); j++) {
                    if (AppController.getInstance().getProductClasses().get(i).getProductClassID() == productModelList.get(j).getProductClassID()) {
                        productModelList.get(j).setHeader(true);
                        if (!hadSection(AppController.getInstance().getProductClasses().get(i).getProductClassID())) {
                            classModels.add(AppController.getInstance().getProductClasses().get(i));
                        }
                    }
                }
            }
            findOthersSectionElements();
        }
    }

    private int findCountForHeader(int headerIndex) {
        int count = 0;
        for (int i = 0; i < productModelList.size(); i++) {
            if (classModels.get(headerIndex).getProductClassID() == 9002) {
                return otherSectionCount;
            } else if (classModels.get(headerIndex).getProductClassID() == productModelList.get(i).getProductClassID()) {
                count++;
            }
        }
        return count;
    }

    private boolean hadSection(int id) {
        for (int i = 0; i < classModels.size(); i++) {
            if (classModels.get(i).getProductClassID() == id) {
                return true;
            }
        }
        return false;
    }

    private void findOthersSectionElements() {
        List<ProductModel> positionWillChange = new ArrayList<>();
        for (int i = 0; i < productModelList.size(); i++) {
            if (!productModelList.get(i).isHeader()) {
                positionWillChange.add(productModelList.get(i));
            }
        }
        otherSectionCount = positionWillChange.size();
        if (otherSectionCount != 0) {
            ProductClassModel model = new ProductClassModel(9002, "Diğerleri");
            classModels.add(model);
            for (int j = 0; j < positionWillChange.size(); j++) {
                productModelList.set(productModelList.size() - 1, positionWillChange.get(j));
            }
        }
        SimpleSectionedGridAdapter.Section[] sections = new SimpleSectionedGridAdapter.Section[classModels.size()];
        for(int i = 0; i < classModels.size(); i++){
            SimpleSectionedGridAdapter.Section section;
            if(i == 0){
                position = 0;
            }else{
                position = position + findCountForHeader(i - 1);
            }

            section = new SimpleSectionedGridAdapter.Section(position, classModels.get(i).getClassName());
            sections[i] = section;
        }

        gridAdapter.setSections(sections);
    }

    public SimpleSectionedGridAdapter getGridAdapter() {
        return gridAdapter;
    }

    public void setGridAdapter(SimpleSectionedGridAdapter gridAdapter) {
        this.gridAdapter = gridAdapter;
    }
}
