package com.opusgo.vapiano.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opusgo.vapiano.R;
import com.opusgo.vapiano.models.CategoryModel;

import java.util.List;

/**
 * Created by hakankanmaz on 5.02.2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private List<CategoryModel> categoryModelList;
Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView lblTitle;
        public LinearLayout layoutTitle;


        public MyViewHolder(View view) {
            super(view);
            lblTitle = (TextView) view.findViewById(R.id.lblTitle);
            layoutTitle = (LinearLayout) view.findViewById(R.id.layoutTitle);

        }
    }


    public CategoryAdapter(List<CategoryModel> categoryModelList, Context context) {
        this.categoryModelList = categoryModelList;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_category, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CategoryModel categoryModel = categoryModelList.get(position);

        if(categoryModel.getName()!=null){
            holder.lblTitle.setText(categoryModel.getName());

        }
        if(categoryModel.getFontColor()!=null){
            holder.lblTitle.setTextColor(Color.parseColor(categoryModel.getFontColor()));
        }

        if(categoryModel.getBackgroundColor()!=null){
            holder.layoutTitle.setBackgroundColor(Color.parseColor(categoryModel.getBackgroundColor()));
        } else {
            holder.layoutTitle.setBackgroundColor(Color.parseColor("#37D551"));

        }

        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        holder.layoutTitle.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));


    }

    @Override
    public int getItemCount() {
        return categoryModelList.size();
    }


    public void addItem(CategoryModel item) {
        categoryModelList.add(item);
    }

    public void clearItems() {
        categoryModelList.clear();
    }

}