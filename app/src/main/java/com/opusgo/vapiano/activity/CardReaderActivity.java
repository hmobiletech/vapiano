package com.opusgo.vapiano.activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.opusgo.vapiano.models.NFCUserModel;
import com.opusgo.vapiano.util.Constants;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by redugsi on 20/02/17.
 */

public abstract class CardReaderActivity extends AppCompatActivity {
    private NfcAdapter mAdapter;
    private PendingIntent mPendingIntent;
    private IntentFilter[] mIntentFilters;
    private String[][] mTechList;
    protected NFCUserModel userModel;
    protected NFCUserModel waiterModel;
    protected boolean fromLogin = false;
    protected boolean alreadyRead = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNFC();
    }

    private void setNFC() {
        mAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mAdapter == null) {
            this.finish();
            return;
        }

        mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter filter = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
        try {
            filter.addDataType("*/*");
        } catch (IntentFilter.MalformedMimeTypeException e) {
            e.printStackTrace();
        }
        mIntentFilters = new IntentFilter[]{filter};
        mTechList = new String[][]{new String[]{MifareClassic.class.getName()}};
    }

    private String performRead(MifareClassic mfc, int blockIndex) throws IOException {
        if (!mfc.isConnected()) {
            mfc.connect();
        }
        String ret = null;

        int sectorIndex = mfc.blockToSector(blockIndex);
        boolean auth = mfc.authenticateSectorWithKeyA(sectorIndex, MifareClassic.KEY_DEFAULT);
        if (auth) {
            byte[] data = readBlock(mfc, blockIndex);
            ret = convertBytes2String(data);
        } else {
            Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
        }

        return ret;
    }

    private byte[] readBlock(MifareClassic mfc, int blockIndex) throws IOException {
        return mfc.readBlock(blockIndex);
    }

    private String convertBytes2String(byte[] data) throws UnsupportedEncodingException {
        String ret;

        int pos = data.length;
        for (int i = data.length - 1; i >= 0; i--) {
            if (data[i] != 0) {
                break;
            }
            pos = i;
        }
        ret = new String(data, 0, pos, Constants.NFC_CHAR_SET_UTF);

        return ret;
    }

    private byte[] convertString2Bytes(String content) throws UnsupportedEncodingException {
        byte[] ret = new byte[16];
        byte[] buf = content.getBytes(Constants.NFC_CHAR_SET_UTF);
        int retLen = ret.length;
        int bufLen = buf.length;
        boolean b = retLen > bufLen;

        for (int i = 0; i < retLen; i++) {
            if (b && i >= bufLen) {
                ret[i] = 0;
                continue;
            }
            ret[i] = buf[i];
        }
        return ret;
    }


    @Override
    protected void onResume() {
        super.onResume();
       mAdapter.enableForegroundDispatch(this, mPendingIntent, mIntentFilters, mTechList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (!NfcAdapter.ACTION_TECH_DISCOVERED.equals(intent.getAction()) || alreadyRead) {
            return;
        }

        MifareClassic mfc = MifareClassic.get((Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG));

        try {
            String cardNo = performRead(mfc, Constants.NFC_CARD_NUMBER_BLOCK);
            if (cardNo != null && !cardNo.isEmpty()){
                cardNo = cardNo.replace("*","");
                alreadyRead = true;
            }else{
                alreadyRead = false;
            }

            String companyNo = performRead(mfc, Constants.NFC_COMPANY_NO_BLOCK).replace("*","");
            String limit = performRead(mfc, Constants.NFC_LIMIT_BLOCK).replace("*","");
            String customerName = performRead(mfc, Constants.NFC_CUSTOMER_NAME_BLOCK).replace("*","");
            String customerSurname = performRead(mfc, Constants.NFC_CUSTOMER_SURNAME_BLOCK).replace("*","");
            String createdDate = performRead(mfc, Constants.NFC_CARD_CREATED_DATE_BLOCK).replace("*","");
            String cardType = performRead(mfc, Constants.NFC_CARD_TYPE_BLOCK).replace("*","");

            if(fromLogin){
                waiterModel = new NFCUserModel(cardNo,companyNo,limit,customerName,customerSurname,createdDate,cardType);
                Log.e("Model","WaiterModel" + cardNo+"-"+companyNo+"-"+limit+"-"+customerName+"-"+customerSurname+"-"+createdDate+"-"+cardType);
            }else{
                userModel = new NFCUserModel(cardNo,companyNo,limit,customerName,customerSurname,createdDate,cardType);
                Log.e("Model","CustomerModel" + cardNo+"-"+companyNo+"-"+limit+"-"+customerName+"-"+customerSurname+"-"+createdDate+"-"+cardType);
            }

            didNFCRead();
        } catch (IOException e) {
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    protected abstract void didNFCRead();

}
