package com.opusgo.vapiano.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.opusgo.vapiano.AppController;
import com.opusgo.vapiano.R;
import com.opusgo.vapiano.adapters.PersonelAdapter;
import com.opusgo.vapiano.api.ApiUtil;
import com.opusgo.vapiano.models.AuthorizedMembersResponse;
import com.opusgo.vapiano.util.Constants;
import com.opusgo.vapiano.util.DialogUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class LoginActivity extends CardReaderActivity {

    PersonelAdapter personelAdapter;
    AuthorizedMembersResponse personelModel;
    List<AuthorizedMembersResponse> personelModelList;
    GridView gridViewPersonel;


    ImageButton btnSettings;
    EditText txtPassword;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppController.setFullScreen(this);
        fromLogin = true;
        setContentView(R.layout.activity_login);

        btnSettings = (ImageButton) findViewById(R.id.btnSettings);

        gridViewPersonel = (GridView) findViewById(R.id.gridViewPersonel);

        personelModelList = new ArrayList<>();

        personelModelList = AppController.getInstance().getPersonelList();
       /* personelModelList.add(new PersonelModel(4, "Metin"));
        personelModelList.add(new PersonelModel(5, "Muhammed"));
        personelModelList.add(new PersonelModel(6, "Burak"));*/


        personelAdapter = new PersonelAdapter(LoginActivity.this, personelModelList, getApplicationContext());
        gridViewPersonel.setAdapter(personelAdapter);


        gridViewPersonel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                personelModel = (AuthorizedMembersResponse) personelAdapter.getItem(position);

                showPasswordDialog(personelModel);
            }
        });


        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSettingsPasswordDialog();
            }
        });
    }

    @Override
    protected void didNFCRead() {
        Log.d("WaiterModel", waiterModel.getCustomerName() + " " + waiterModel.getCustomerSurname());
        AuthorizedMembersResponse personal = new AuthorizedMembersResponse();
        personal.setFullName(waiterModel.getCustomerName() + " " + waiterModel.getCustomerSurname());
        boolean isSucced = false;
        for (int i = 0; i < personelModelList.size(); i++) {
            if (personelModelList.get(i).getFullName().equals(personal.getFullName())) {
                AppController.getInstance().setWaiter(personal);
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                isSucced = true;
            }
        }

        if (!isSucced) {
            DialogUtil.showErrorDialog(LoginActivity.this, getResources().getString(R.string.please_check_your_information));
        }
    }

    private void showPasswordDialog(final AuthorizedMembersResponse authorizedMembersResponse) {
        final Dialog dialog = new Dialog(LoginActivity.this, android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //  dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_password);

        // set the custom dialog components - text, image and button
        CircleImageView imgPersonel = (CircleImageView) dialog.findViewById(R.id.imgPersonel);
        TextView lblPersonelName = (TextView) dialog.findViewById(R.id.lblPersonelName);
        txtPassword = (EditText) dialog.findViewById(R.id.txtPassword);
        TextView btnOne = (TextView) dialog.findViewById(R.id.btnOne);
        TextView btnTwo = (TextView) dialog.findViewById(R.id.btnTwo);
        TextView btnThree = (TextView) dialog.findViewById(R.id.btnThree);
        TextView btnFour = (TextView) dialog.findViewById(R.id.btnFour);
        TextView btnFive = (TextView) dialog.findViewById(R.id.btnFive);
        TextView btnSix = (TextView) dialog.findViewById(R.id.btnSix);
        TextView btnSeven = (TextView) dialog.findViewById(R.id.btnSeven);
        TextView btnEight = (TextView) dialog.findViewById(R.id.btnEight);
        TextView btnNine = (TextView) dialog.findViewById(R.id.btnNine);
        TextView btnZero = (TextView) dialog.findViewById(R.id.btnZero);
        ImageButton btnSignIn = (ImageButton) dialog.findViewById(R.id.btnSignIn);
        ImageButton btnDelete = (ImageButton) dialog.findViewById(R.id.btnDelete);
        ImageButton btnCancel = (ImageButton) dialog.findViewById(R.id.btnCancel);


        lblPersonelName.setText(authorizedMembersResponse.getFullName());

        Picasso.with(getApplicationContext())
                .load(ApiUtil.BASE_IMAGE_URL+authorizedMembersResponse.getPhotoImageUrl())
                .placeholder(R.drawable.no_avatar)
                .error(R.drawable.no_avatar)
                .into(imgPersonel);

        btnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("1");
            }
        });

        btnTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("2");

            }
        });

        btnThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("3");

            }
        });

        btnFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("4");

            }
        });

        btnFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("5");

            }
        });

        btnSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("6");

            }
        });

        btnSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("7");

            }
        });

        btnEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("8");

            }
        });

        btnNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("9");

            }
        });

        btnZero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("0");

            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* password = numberList.toString().replace("[", "").replace("]", "").replace(", ", "");
                Log.e("ŞİFRE", password);*/

                if (txtPassword.getText().toString().isEmpty()) {
                    DialogUtil.showInfoDialog(LoginActivity.this, getResources().getString(R.string.please_check_your_password));
                } else if (!authorizedMembersResponse.getPass().equals(txtPassword.getText().toString())) {
                    DialogUtil.showInfoDialog(LoginActivity.this, getResources().getString(R.string.please_check_your_password));
                } else if (authorizedMembersResponse.getPass().equals(txtPassword.getText().toString())) {
                    AppController.getInstance().setWaiter(authorizedMembersResponse);
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                    dialog.dismiss();
                }

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txtPassword.getText().toString().length() > 0)
                    txtPassword.setText(txtPassword.getText().delete(txtPassword.getText().length() - 1, txtPassword.getText().length()));
                /*if (number != 0) {
                    numberList.remove(number - 1);
                    number--;
                    Log.e("List", String.valueOf(numberList));

                }

                controlNumber();*/

            }
        });


        dialog.show();
    }

    private void appendNumber(String number) {
        txtPassword.append(number);
    }


    private void showSettingsPasswordDialog() {
        final Dialog dialog = new Dialog(LoginActivity.this, android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //  dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_settings_password);

        // set the custom dialog components - text, image and button
        txtPassword = (EditText) dialog.findViewById(R.id.txtPassword);
        TextView btnOne = (TextView) dialog.findViewById(R.id.btnOne);
        TextView btnTwo = (TextView) dialog.findViewById(R.id.btnTwo);
        TextView btnThree = (TextView) dialog.findViewById(R.id.btnThree);
        TextView btnFour = (TextView) dialog.findViewById(R.id.btnFour);
        TextView btnFive = (TextView) dialog.findViewById(R.id.btnFive);
        TextView btnSix = (TextView) dialog.findViewById(R.id.btnSix);
        TextView btnSeven = (TextView) dialog.findViewById(R.id.btnSeven);
        TextView btnEight = (TextView) dialog.findViewById(R.id.btnEight);
        TextView btnNine = (TextView) dialog.findViewById(R.id.btnNine);
        TextView btnZero = (TextView) dialog.findViewById(R.id.btnZero);
        ImageButton btnSignIn = (ImageButton) dialog.findViewById(R.id.btnSignIn);
        ImageButton btnDelete = (ImageButton) dialog.findViewById(R.id.btnDelete);
        ImageButton btnCancel = (ImageButton) dialog.findViewById(R.id.btnCancel);


        btnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("1");
            }
        });

        btnTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("2");

            }
        });

        btnThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("3");

            }
        });

        btnFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("4");

            }
        });

        btnFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("5");

            }
        });

        btnSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("6");

            }
        });

        btnSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("7");

            }
        });

        btnEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("8");

            }
        });

        btnNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("9");

            }
        });

        btnZero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendNumber("0");

            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txtPassword.getText().toString().equals(Constants.SETTING_PASSWORD)) {
                    startActivity(new Intent(LoginActivity.this, SettingsActivity.class));
                    dialog.dismiss();
                } else {
                    DialogUtil.showInfoDialog(LoginActivity.this, getResources().getString(R.string.please_check_your_password));
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtPassword.getText().toString().length() > 0)
                    txtPassword.setText(txtPassword.getText().delete(txtPassword.getText().length() - 1, txtPassword.getText().length()));

            }
        });


        dialog.show();
    }

}


