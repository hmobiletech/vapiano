package com.opusgo.vapiano.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.opusgo.vapiano.AppController;
import com.opusgo.vapiano.R;
import com.opusgo.vapiano.adapters.OrderHistoryAdapter;
import com.opusgo.vapiano.api.Service;
import com.opusgo.vapiano.listeners.ResponseListener;
import com.opusgo.vapiano.models.OrderModel;
import com.opusgo.vapiano.models.OrderProductListModel;
import com.opusgo.vapiano.models.OrderProductModel;
import com.opusgo.vapiano.util.SharedPreferencesUtil;

import java.util.ArrayList;

public class OrderHistoryActivity extends AppCompatActivity {


    private RecyclerView listOrderHistory;
    private OrderHistoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.setFullScreen(this);
        setContentView(R.layout.activity_order_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init(){
        listOrderHistory = (RecyclerView)findViewById(R.id.listOrderHistory);
        adapter = new OrderHistoryAdapter(null);
        listOrderHistory.setLayoutManager(new LinearLayoutManager(this));
        listOrderHistory.setItemAnimator(new DefaultItemAnimator());
        listOrderHistory.setAdapter(adapter);

        GetOrderHistoryTask task = new GetOrderHistoryTask(Long.valueOf(SharedPreferencesUtil.getWorkStationID(getApplicationContext())), new ResponseListener<OrderProductListModel>() {
            @Override
            public void onSuccess(OrderProductListModel object) {
                Log.v("OK","OrderHistorySuccess");
                fetchAdapter(object);
            }

            @Override
            public void onError(String message) {

            }
        });
        task.execute();
    }

    private void fetchAdapter(OrderProductListModel list){
        ArrayList<OrderProductModel> orders = new ArrayList<>();
        for (OrderModel first : list){
            for (OrderProductModel second : first.getOrderProducts()){
                orders.add(second);
            }
        }
        adapter.setOrders(orders);
        adapter.notifyDataSetChanged();
    }

    public class GetOrderHistoryTask extends AsyncTask<Void,Void,Void>{

        private long workStationId;
        private ResponseListener<OrderProductListModel> listener;

        public GetOrderHistoryTask(long workStationId, ResponseListener<OrderProductListModel> listener){
            this.workStationId = workStationId;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Service.getOrdersByTerminalID(OrderHistoryActivity.this, workStationId, new ResponseListener<OrderProductListModel>() {
                @Override
                public void onSuccess(OrderProductListModel object) {
                    listener.onSuccess(object);
                }

                @Override
                public void onError(String message) {
                    listener.onError(message);
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }
}
