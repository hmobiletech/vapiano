package com.opusgo.vapiano.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.opusgo.vapiano.AppController;
import com.opusgo.vapiano.R;
import com.opusgo.vapiano.adapters.CategoryAdapter;
import com.opusgo.vapiano.adapters.OrderListAdapter;
import com.opusgo.vapiano.adapters.ProductAdapter;
import com.opusgo.vapiano.api.ApiUtil;
import com.opusgo.vapiano.api.Service;
import com.opusgo.vapiano.listeners.CategoryListener;
import com.opusgo.vapiano.listeners.ClickListener;
import com.opusgo.vapiano.listeners.ProductListener;
import com.opusgo.vapiano.listeners.RecyclerTouchListener;
import com.opusgo.vapiano.listeners.RemovePriceListener;
import com.opusgo.vapiano.listeners.ResponseListener;
import com.opusgo.vapiano.models.CategoryListModel;
import com.opusgo.vapiano.models.CategoryModel;
import com.opusgo.vapiano.models.OrderListModel;
import com.opusgo.vapiano.models.OrderModel;
import com.opusgo.vapiano.models.OrderProductListModel;
import com.opusgo.vapiano.models.OrderProductModel;
import com.opusgo.vapiano.models.ProductClassModel;
import com.opusgo.vapiano.models.ProductListModel;
import com.opusgo.vapiano.models.ProductModel;
import com.opusgo.vapiano.util.DialogUtil;
import com.opusgo.vapiano.util.SharedPreferencesUtil;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import dev.dworks.libs.astickyheader.SimpleSectionedGridAdapter;

public class MainActivity extends CardReaderActivity
        implements NavigationView.OnNavigationItemSelectedListener, RemovePriceListener {


    ProgressDialog progress;
    ImageButton btnCloseMenu;
    List<CategoryModel> categoryModelList = new ArrayList<>();
    RecyclerView recyclerViewCategory;
    CategoryAdapter categoryAdapter;
    CategoryModel categoryModel;

    ProductAdapter productAdapter;
    ProductModel productModel;
    List<ProductModel> productModelList = new ArrayList<>();
    GridView gridViewProduct;
    SimpleSectionedGridAdapter gridAdapter;
    private List<ProductClassModel> classModels;
    int otherSectionCount;


    OrderListAdapter orderListAdapter;
    OrderListModel orderListModel;
    ArrayList<ProductModel> orderListModelList = new ArrayList<>();
    RecyclerView recyclerViewOrderList;


    TextView lblPersonelName;
    TextView lblTimer;
    TextView lblDateTime;
    TextView lblCardNo;
    TextView lblTotalPrice;
    CircleImageView imgPersonel;
    Button btnReset;
    Button btnOrderHistory;
    Button btnSendOrder;
    CountDownTimer countDownTimer;
    String timerTimeout;

    private long orderId;

    double totalPrice = 0.0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppController.setFullScreen(this);
        fromLogin = false;
//        Service.getMember(this, 2, new ResponseListener<PersonelModel>() {
//            @Override
//            public void onSuccess(PersonelModel object) {
//
//            }
//
//            @Override
//            public void onError(String message) {
//
//            }
//        });
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        classModels = new ArrayList<>();
        progress = new ProgressDialog(this);
        progress.setTitle("Lütfen bekleyiniz");
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //drawer.setDrawerListener(toggle);

        toggle.syncState();

        toggle.setDrawerIndicatorEnabled(false);

        // mDrawerToggle.setHomeAsUpIndicator(R.drawable.menu_icon);

        toolbar.setNavigationIcon(R.drawable.menu);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        lblPersonelName = (TextView) findViewById(R.id.lblPersonelName);
        lblCardNo = (TextView) findViewById(R.id.lblCardNo);
        lblTotalPrice = (TextView) findViewById(R.id.lblTotalPrice);
        lblTimer = (TextView) findViewById(R.id.lblTimer);
        lblDateTime = (TextView) findViewById(R.id.lblDateTime);
        imgPersonel = (CircleImageView) findViewById(R.id.imgPersonel);
        btnReset = (Button) findViewById(R.id.btnReset);
        btnCloseMenu = (ImageButton) findViewById(R.id.btnCloseMenu);
        btnOrderHistory = (Button) findViewById(R.id.btnOrderHistory);
        btnSendOrder = (Button) findViewById(R.id.btnSendOrder);

     /*   TextView waiterName = (TextView)findViewById(R.id.waiter_name);
        waiterName.setText(AppController.getInstance().getWaiter().getName() + " " + AppController.getInstance().getWaiter().getLastName());
     */
        recyclerViewCategory = (RecyclerView) findViewById(R.id.recyclerViewCategory);
        categoryAdapter = new CategoryAdapter(categoryModelList, getApplicationContext());
        RecyclerView.LayoutManager mLayoutManagerCategoryList = new LinearLayoutManager(getApplicationContext());
        recyclerViewCategory.setLayoutManager(mLayoutManagerCategoryList);
        recyclerViewCategory.setItemAnimator(new DefaultItemAnimator());
        recyclerViewCategory.setAdapter(categoryAdapter);

        btnCloseMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        recyclerViewCategory.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerViewCategory, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                categoryModel = categoryModelList.get(position);
                getProducts(categoryModel.getCategoryID(), categoryModel.getBackgroundColor());
                startTimer();
                onBackPressed();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        gridViewProduct = (GridView) findViewById(R.id.gridViewProduct);
        RecyclerView.LayoutManager mLayoutManagerOrderList = new LinearLayoutManager(getApplicationContext());
        productAdapter = new ProductAdapter(MainActivity.this, productModelList, getApplicationContext());
        gridAdapter = new SimpleSectionedGridAdapter(this, productAdapter, R.layout.grid_item_header, R.id.header_layout, R.id.header);
        productAdapter.setGridAdapter(gridAdapter);
        gridAdapter.setGridView(gridViewProduct);
        gridViewProduct.setAdapter(gridAdapter);

        recyclerViewOrderList = (RecyclerView) findViewById(R.id.recyclerViewOrderList);
        orderListAdapter = new OrderListAdapter(orderListModelList, this);
        recyclerViewOrderList.setLayoutManager(mLayoutManagerOrderList);
        recyclerViewOrderList.setItemAnimator(new DefaultItemAnimator());
        recyclerViewOrderList.setAdapter(orderListAdapter);


        gridViewProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("ClickPosition", position + "");
                productModel = (ProductModel) gridAdapter.getItem(position);
                orderListAdapter.addItem(productModel);
                orderListAdapter.notifyDataSetChanged();
                startTimer();

                if (btnReset.getVisibility() == View.INVISIBLE) {
                    visibleResetButton();
                }

                calculatePrice();
            }
        });


        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });



        imgPersonel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });

        btnOrderHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, OrderHistoryActivity.class));
            }
        });


        btnSendOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOrder();
            }
        });

        init();
        getCategories();
    }

    private void reset(){
        alreadyRead = false;
        resetOrderList();
        //  startTimer();
        stopTimer();
        resetBackgroundTimer();
        lblTimer.setText("0");
        invisibleResetButton();
        userModel = null;
        resetCardNoText();
        resetTotalPrice();
    }

    private void sendOrder(){
        if (userModel == null){
            DialogUtil.showErrorDialog(MainActivity.this,"Müşteri kartını okutunuz");
            return;
        }
        ArrayList<OrderProductModel> orderProducts = new ArrayList<>();
        for (ProductModel product : orderListModelList){

//            OrderModel orderModel = new OrderModel(0,Long.valueOf(SharedPreferencesUtil.getWorkStationID(getApplicationContext())),
//                    AppController.getInstance().getWaiter().getMemberID(), userModel.getCardNumber(),"Sipariş notuu","2017-03-12T20:47:46.903",)
            //long orderProductID, long productID, double productQuantity, String productName, double productPrice, String productNote, String createdDateTime
            if (product.getOrderProductId() == 0){
                OrderProductModel orderProductModel = new OrderProductModel(orderId,product.getProductID(),
                        1,product.getName(),product.getListPrice(),"Product note","");
                orderProducts.add(orderProductModel);
            }
        }
        OrderModel model = new OrderModel(orderId, Long.valueOf(SharedPreferencesUtil.getWorkStationID(getApplicationContext())),AppController.getInstance().getWaiter().getMemberID(),userModel.getCardNumber()
        ,"Order Note","",orderProducts);
        SendOrderTask task = new SendOrderTask(model, new ResponseListener<Object>() {
            @Override
            public void onSuccess(Object object) {
                Log.v("OK","SendOrder");
                DialogUtil.showSuccessDialog(MainActivity.this,"Sipariş gönderildi");
                reset();
            }

            @Override
            public void onError(String message) {
                Log.v("Error","SendOrder");
                DialogUtil.showErrorDialog(MainActivity.this,"Birşeyler ters gitti");
            }
        });
        task.execute();
    }
    private void resetTotalPrice() {
        totalPrice = 0;
        lblTotalPrice.setText(totalPrice + " TRY");
    }

    private void calculatePrice() {
        totalPrice = totalPrice + productModel.getListPrice();
        lblTotalPrice.setText(totalPrice + " TRY");
    }

    private void logout() {
        userModel = null;
        waiterModel = null;
        finish();
        alreadyRead = false;
        AppController.getInstance().setWaiter(null);
        startActivity(new Intent(this, LoginActivity.class));
    }

    @Override
    protected void didNFCRead() {
        //First reading
        if (!lblCardNo.getText().toString().contains(userModel.getCardNumber())){
            lblCardNo.setText(getResources().getString(R.string.card_no) + " " + userModel.getCardNumber());
            orderId = 0;
            getUnpayedOrders();
        }
        Log.v("SendOrder",Long.valueOf(SharedPreferencesUtil.getWorkStationID(getApplicationContext()))+"");

    }

    private void getUnpayedOrders(){
        GetUnpayedOrders getUnpayedOrders = new GetUnpayedOrders(userModel.getCardNumber(), new ResponseListener<OrderProductListModel>() {
            @Override
            public void onSuccess(OrderProductListModel object) {
                if (object == null){
                    return;
                }

                for (OrderModel model : object){
                    orderId = model.getOrderID();
                    for (OrderProductModel orderModel : model.getOrderProducts()){
//                        private long OrderProductID;
//                        private long ProductID;
//                        private double ProductQuantity;
//                        private String ProductName;
//                        private double ProductPrice;
//                        private String ProductNote;
//                        private String CreatedDateTime;
                        ProductModel productModel = new ProductModel();
                        productModel.setProductID(orderModel.getProductID());
                        productModel.setName(orderModel.getProductName());
                        productModel.setListPrice(orderModel.getProductPrice());
                        productModel.setCreated(orderModel.getCreatedDateTime());
                        productModel.setOrderProductId(orderId);

                        for (int i = 0; i < orderModel.getProductQuantity(); i++){
                            orderListModelList.add(productModel);

                            totalPrice = totalPrice + productModel.getListPrice();
                            lblTotalPrice.setText(totalPrice + " TRY");

                        }
                    }
                }

                orderListAdapter.notifyDataSetChanged();

            }

            @Override
            public void onError(String message) {

            }
        });

        getUnpayedOrders.execute();
    }

    private void startTimer() {
        stopTimer();
        countDownTimer.start();
        lblTimer.setBackground(getResources().getDrawable(R.drawable.timer_square_orange));

    }

    private void stopTimer() {
        countDownTimer.cancel();
    }

    private void setTimer() {
        int count;
        if (!timerTimeout.isEmpty()) {
            count = Integer.parseInt(timerTimeout) * 1000;
        } else {
            count = 25000;
        }
        countDownTimer = new CountDownTimer(count, 1000) {
            public void onTick(long millisUntilFinished) {
                lblTimer.setText("" + millisUntilFinished / 1000);

            }

            public void onFinish() {
                if (!timerTimeout.isEmpty()) {
                    lblTimer.setText(timerTimeout);
                } else {
                    lblTimer.setText("0");
                }
                resetOrderList();
                resetBackgroundTimer();
                // startTimer();
                invisibleResetButton();
                resetTotalPrice();
            }
        };
    }

    private void resetBackgroundTimer() {
        lblTimer.setBackground(getResources().getDrawable(R.drawable.timer_square_green));
    }

    public void resetOrderList() {
        orderListModelList.clear();
        orderListAdapter.clearItems();
        recyclerViewOrderList.removeAllViews();
    }

    private void init() {

        lblPersonelName.setText(AppController.getInstance().getWaiter().getFullName());
        Picasso.with(getApplicationContext())
                .load(ApiUtil.BASE_IMAGE_URL+AppController.getInstance().getWaiter().getPhotoImageUrl())
                .placeholder(R.drawable.no_avatar)
                .error(R.drawable.no_avatar)
                .into(imgPersonel);
        invisibleResetButton();
        timerTimeout = SharedPreferencesUtil.getTimerTimeout(getApplicationContext());
        resetCardNoText();
        lblTotalPrice.setText(getResources().getString(R.string.default_total_price));
        if (!timerTimeout.isEmpty()) {
            lblTimer.setText(timerTimeout);
        } else {
            lblTimer.setText("0");
        }
        lblDateTime.setText(getTime());
        setTimer();
    }

    private void resetCardNoText() {
        lblCardNo.setText(getResources().getString(R.string.card_no) + " " + getResources().getString(R.string.please_read_card));
    }

    private void invisibleResetButton() {
        btnReset.setVisibility(View.INVISIBLE);
    }

    private void visibleResetButton() {
        btnReset.setVisibility(View.VISIBLE);
    }

    private String getTime() {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String str = sdf.format(new Date());

        return str;
    }

    private void getCategories() {
        Service.getCategories(getApplicationContext(), new CategoryListener<CategoryListModel>() {
            @Override
            public void onSuccess(CategoryListModel result) {
                for (CategoryModel entity : result) {
                    categoryAdapter.addItem(entity);
                }
                categoryAdapter.notifyDataSetChanged();
                getProducts(result.get(0).getCategoryID(), result.get(0).getBackgroundColor());

            }


            @Override
            public void onError(String message) {

            }
        });
    }

    private void getProducts(final int categoryId, final String categoryColor) {

        if (productAdapter != null) {
            productAdapter.clearItems();
        }

        Service.getProductByCategory(categoryId, getApplicationContext(), new ProductListener<ProductListModel>() {
            @Override
            public void onSuccess(ProductListModel result) {
                for (ProductModel entity : result) {
                    productAdapter.addItem(entity);
                }

                gridViewProduct.setBackgroundColor(Color.parseColor(categoryColor));
                productAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String message) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onRemove(double price) {
        totalPrice = totalPrice - price;
        lblTotalPrice.setText(totalPrice + " TRY");
    }

    public class SendOrderTask extends AsyncTask<Void,Void,Void>{

        private OrderModel order;
        private ResponseListener<Object> listener;

        public SendOrderTask(OrderModel order, ResponseListener<Object> listener){
            this.order = order;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
//            if (progress != null)
//            if (!progress.isShowing()){
//                progress.show();
//            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (order != null){
                Service.order(MainActivity.this, order, new ResponseListener<Object>() {
                    @Override
                    public void onSuccess(Object object) {
                        listener.onSuccess(object);
                    }

                    @Override
                    public void onError(String message) {
                        listener.onError(message);
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
//            if (progress != null)
//                if (progress.isShowing())
//                    progress.hide();
        }
    }

    public class GetUnpayedOrders extends AsyncTask<Void,Void,Void>{

        private String cardNumber;
        private ResponseListener<OrderProductListModel> listener;

        public GetUnpayedOrders(String cardNumber, ResponseListener<OrderProductListModel> listener){
            this.cardNumber = cardNumber;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
//            if (progress != null)
//                if (!progress.isShowing()){
//                    progress.show();
//                }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Service.getUnpayedOrders(cardNumber, new ResponseListener<OrderProductListModel>() {
                @Override
                public void onSuccess(OrderProductListModel object) {
                    listener.onSuccess(object);
                }

                @Override
                public void onError(String message) {
                    listener.onError(message);
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
//            if (progress != null)
//                if (progress.isShowing())
//                    progress.hide();
        }
    }
}
