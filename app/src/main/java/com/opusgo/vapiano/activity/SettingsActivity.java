package com.opusgo.vapiano.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.opusgo.vapiano.AppController;
import com.opusgo.vapiano.R;
import com.opusgo.vapiano.util.SharedPreferencesUtil;

import java.io.IOException;

public class SettingsActivity extends AppCompatActivity {

    EditText txtLanguageID;
    EditText txtCompanyID;
    EditText txtWorkStationID;
    EditText txtCurrencyID;
    EditText txtMachineNo;
    EditText txtTimerTimeout;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.setFullScreen(this);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        txtLanguageID = (EditText) findViewById(R.id.txtLanguageID);
        txtCompanyID = (EditText) findViewById(R.id.txtCompanyID);
        txtWorkStationID = (EditText) findViewById(R.id.txtWorkStationID);
        txtCurrencyID = (EditText) findViewById(R.id.txtCurrencyID);
        txtMachineNo = (EditText) findViewById(R.id.txtMachineNo);
        txtTimerTimeout = (EditText) findViewById(R.id.txtTimerTimeout);
        btnSave = (Button) findViewById(R.id.btnSave);


        getInformation();


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    SharedPreferencesUtil.setLanguageID(getApplicationContext(), txtLanguageID.getText().toString());
                    SharedPreferencesUtil.setCompanyID(getApplicationContext(), txtCompanyID.getText().toString());
                    SharedPreferencesUtil.setWorkStationID(getApplicationContext(), txtWorkStationID.getText().toString());
                    SharedPreferencesUtil.setCurrencyID(getApplicationContext(), txtCurrencyID.getText().toString());
                    SharedPreferencesUtil.setMachineNo(getApplicationContext(), txtMachineNo.getText().toString());
                    SharedPreferencesUtil.setTimerTimeout(getApplicationContext(), txtTimerTimeout.getText().toString());

                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.completed_successfully), Toast.LENGTH_LONG).show();

                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });


    }

    private void getInformation() {
        txtLanguageID.setText(SharedPreferencesUtil.getLanguageID(getApplicationContext()));
        txtCompanyID.setText(SharedPreferencesUtil.getCompanyID(getApplicationContext()));
        txtWorkStationID.setText(SharedPreferencesUtil.getWorkStationID(getApplicationContext()));
        txtCurrencyID.setText(SharedPreferencesUtil.getCurrencyID(getApplicationContext()));
        txtMachineNo.setText(SharedPreferencesUtil.getMachineNo(getApplicationContext()));
        txtTimerTimeout.setText(SharedPreferencesUtil.getTimerTimeout(getApplicationContext()));
    }

}
