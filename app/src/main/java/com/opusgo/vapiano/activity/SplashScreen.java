package com.opusgo.vapiano.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.opusgo.vapiano.AppController;
import com.opusgo.vapiano.ConnectivityReceiver;
import com.opusgo.vapiano.R;
import com.opusgo.vapiano.api.Service;
import com.opusgo.vapiano.listeners.LoginListener;
import com.opusgo.vapiano.listeners.ResponseListener;
import com.opusgo.vapiano.listeners.XMLListener;
import com.opusgo.vapiano.models.AuthorizedMembersResponse;
import com.opusgo.vapiano.models.AuthorizedMembersResponseList;
import com.opusgo.vapiano.models.OrderModel;
import com.opusgo.vapiano.models.OrderProductListModel;
import com.opusgo.vapiano.models.OrderProductModel;
import com.opusgo.vapiano.models.ProductClassListModel;
import com.opusgo.vapiano.util.CustomDialog;
import com.opusgo.vapiano.util.DialogUtil;
import com.opusgo.vapiano.util.SharedPreferencesUtil;

import java.util.ArrayList;

public class SplashScreen extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {


    String deviceCode;
    long workStationID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.setFullScreen(this);
        setContentView(R.layout.activity_splash_screen);





    }

    private void getAllClasses() {
        Service.getAllClasses(new ResponseListener<ProductClassListModel>() {
            @Override
            public void onSuccess(ProductClassListModel object) {
                AppController.getInstance().setProductClasses(object);
            }

            @Override
            public void onError(String message) {

            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        ConnectivityReceiver.showErrorConnection(this, isConnected);

    }


    public void controlXML(XMLListener listener) {
        if (SharedPreferencesUtil.getMachineNo(getApplicationContext()).isEmpty() || SharedPreferencesUtil.getWorkStationID(getApplicationContext()).isEmpty()) {
            listener.onError("Eksik XML");
        } else {
            listener.onSuccess();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected(getApplicationContext())) {
            controlXML(new XMLListener() {
                @Override
                public void onSuccess() {

                    deviceCode = SharedPreferencesUtil.getMachineNo(getApplicationContext());
                    workStationID = Long.parseLong(SharedPreferencesUtil.getWorkStationID(getApplicationContext()));
                    getAllClasses();

                    Service.getAuthorizedMembers(getApplicationContext(), deviceCode,workStationID , new LoginListener<AuthorizedMembersResponseList>() {

                        @Override
                        public void onSuccess(AuthorizedMembersResponseList object) {
                            AppController.getInstance().setPersonelList(object);
                            Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                            startActivity(i);
                            finish();
                        }

                        @Override
                        public void onError(String message) {
                            DialogUtil.showSuccessDialog(SplashScreen.this, message);
                        }
                    });
                }

                @Override
                public void onError(String message) {
                    final CustomDialog c = new CustomDialog(CustomDialog.DIALOG_INFO, 2, SplashScreen.this, message);
                    c.setEvetButtonText(getResources().getString(R.string.yes));
                    c.setEvetOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(SplashScreen.this, SettingsActivity.class));
                            c.dismiss();
                        }
                    });
                    c.setHayirButtonText(getResources().getString(R.string.no));
                    c.setHayirOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            c.dismiss();
                        }
                    });
                    c.init();
                    c.show();
                }
            });
        } else {
            ConnectivityReceiver.checkConnection(this, getApplicationContext());
        }
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */
}
