package com.opusgo.vapiano;

/**
 * Created by hakankanmaz
 */

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;

import com.opusgo.vapiano.util.DialogUtil;


public class ConnectivityReceiver
        extends BroadcastReceiver {

    public static ConnectivityReceiverListener connectivityReceiverListener;

    public ConnectivityReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent arg1) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();

        if (connectivityReceiverListener != null) {
            connectivityReceiverListener.onNetworkConnectionChanged(isConnected);
        }
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager
                cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }


    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }

    public static void checkConnection(Activity activity, Context context) {
        boolean isConnected = ConnectivityReceiver.isConnected(context);
        showErrorConnection(activity,isConnected);
    }

    public static void showErrorConnection(final Activity activity, boolean isConnected){

        if (!isConnected) {

            DialogUtil.showInternetWarningDialog(activity,activity.getResources().getString(R.string.connection_Error_Warning));
            DialogUtil.showErrorDialog(activity, activity.getResources().getString(R.string.connection_Error_Warning), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   activity.finish();
                }
            });
        }
    }
}