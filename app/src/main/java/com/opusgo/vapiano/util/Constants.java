package com.opusgo.vapiano.util;

/**
 * Created by hakankanmaz on 7.02.2017.
 */

public class Constants {

    public static final String CONTENT_TYPE="Content-Type";
    public static final String ACCEPT="Accept";

    public static final String APPLICATION_JSON="application/json";

    public static final String NFC_CHAR_SET_UTF = "UTF-8";
    public static final int NFC_CARD_NUMBER_BLOCK = 1;
    public static final int NFC_COMPANY_NO_BLOCK = 2;
    public static final int NFC_LIMIT_BLOCK = 4;
    public static final int NFC_CUSTOMER_NAME_BLOCK = 6;
    public static final int NFC_CUSTOMER_SURNAME_BLOCK = 8;
    public static final int NFC_CARD_CREATED_DATE_BLOCK = 9;
    public static final int NFC_CARD_TYPE_BLOCK = 16;


    public static final int TIMEOUT = 15000;
    public static final String SETTING_PASSWORD = "1234";


}
