package com.opusgo.vapiano.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface.OnDismissListener;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opusgo.vapiano.R;


public class CustomDialog {
    public static final int DIALOG_ERROR = 1;
    public static final int DIALOG_WARNING = 2;
    public static final int DIALOG_INFO = 3;
    public static final int DIALOG_POSITIVE = 4;
    public static final int DIALOG_CONFIRM = 5;
    public static final int DIALOG_INTERNET_WARNING = 6;

    private Dialog c;
    private int dialogType;
    private int buttonNumber;
    private Activity activity;
    private View.OnClickListener evetOnClickListener;
    private View.OnClickListener hayirOnClickListener;
    private View.OnClickListener tamamOnClickListener;
    private OnDismissListener onDismissListener;
    private String evetButtonText;
    private String hayirButtonText;
    private String tamamButtonText;
    private String title;
    private String message;
    private int messageResourceId;
    private boolean isDisableHayirButton;

    public CustomDialog(int dialogType, int buttonNumber, Activity activity, String message) {
        this.dialogType = dialogType;
        this.buttonNumber = buttonNumber;
        this.message = message;
        this.activity = activity;
    }

    public CustomDialog(int dialogType, int buttonNumber, Activity activity, int messageResourceId) {
        this.dialogType = dialogType;
        this.buttonNumber = buttonNumber;
        this.messageResourceId = messageResourceId;
        this.message = activity.getResources().getString(messageResourceId);
        this.activity = activity;
    }

    public void disableHayirButton() {
        isDisableHayirButton = true;
    }

    public void init() {
        c = new Dialog(activity, R.style.DialogTheme);
        c.setContentView(R.layout.custom_dialog_layout);

        LinearLayout oneButtonContainer = (LinearLayout) c.findViewById(R.id.dialog_one_button_container);
        LinearLayout twoButtonContainer = (LinearLayout) c.findViewById(R.id.dialog_two_button_container);

     //   ImageView dialogIcon = (ImageView) c.findViewById(R.id.dialog_icon);
        TextView dialogTitle = (TextView) c.findViewById(R.id.dialog_title);

        if (dialogType == DIALOG_ERROR) {
            //  dialogIcon.setImageResource(R.drawable.error);

            if (title == null)
                dialogTitle.setText(activity.getResources().getString(R.string.error));
        } else if (dialogType == DIALOG_WARNING) {
            //  dialogIcon.setImageResource(R.drawable.warning);

            if (title == null)
                dialogTitle.setText(activity.getResources().getString(R.string.warning));
        } else if (dialogType == DIALOG_INFO) {
            //  dialogIcon.setImageResource(R.drawable.info);

            if (title == null)
                dialogTitle.setText(activity.getResources().getString(R.string.information));
        } else if (dialogType == DIALOG_POSITIVE) {
            //dialogIcon.setImageResource(R.drawable.ok);

            if (title == null)
                dialogTitle.setText(activity.getResources().getString(R.string.positive));
        } else if (dialogType == DIALOG_CONFIRM) {
            //    dialogIcon.setImageResource(R.drawable.info);

            if (title == null)
                dialogTitle.setText(activity.getResources().getString(R.string.confirmation));
        } else if (dialogType == DIALOG_INTERNET_WARNING) {
            //    dialogIcon.setImageResource(R.drawable.warning);

            if (title == null)
                dialogTitle.setText(activity.getResources().getString(R.string.warning));
        }
        if (title != null)
            dialogTitle.setText(title);

        TextView content = (TextView) c.findViewById(R.id.dialog_content);
        content.setText(Html.fromHtml(message));
        content.setMovementMethod(LinkMovementMethod.getInstance());

        if (buttonNumber == 1) {
            oneButtonContainer.setVisibility(View.VISIBLE);
            twoButtonContainer.setVisibility(View.GONE);

            Button tamamButton = (Button) c.findViewById(R.id.dialog_tamam_button);
            if (tamamButtonText != null)
                tamamButton.setText(tamamButtonText);

            if (tamamOnClickListener == null)
                tamamOnClickListener = new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        c.dismiss();
                    }
                };

            tamamButton.setOnClickListener(tamamOnClickListener);
        } else if (buttonNumber == 2) {
            oneButtonContainer.setVisibility(View.GONE);
            twoButtonContainer.setVisibility(View.VISIBLE);

            Button evetButton = (Button) c.findViewById(R.id.dialog_evet_button);
            if (evetButtonText != null)
                evetButton.setText(evetButtonText);

            if (evetOnClickListener == null)
                evetOnClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        c.dismiss();
                    }
                };

            evetButton.setOnClickListener(evetOnClickListener);


            Button hayirButton = (Button) c.findViewById(R.id.dialog_hayir_button);
            if (hayirButtonText != null)
                hayirButton.setText(hayirButtonText);

            if (hayirOnClickListener == null)
                hayirOnClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        c.dismiss();
                    }
                };

            hayirButton.setOnClickListener(hayirOnClickListener);

            if (isDisableHayirButton) {
                hayirButton.setVisibility(View.GONE);
            }
        }

        if (onDismissListener != null)
            c.setOnDismissListener(onDismissListener);
    }

  /*  public void enableDontShowMessage(CompoundButton.OnCheckedChangeListener checkedChangeListener) {
        View dont_show_again_layout = c.findViewById(R.id.dont_show_again_layout);
        dont_show_again_layout.setVisibility(View.VISIBLE);
        CheckBox checkBox = (CheckBox) c.findViewById(R.id.dont_show_again);
        checkBox.setOnCheckedChangeListener(checkedChangeListener);
    }*/

    public void show() {

        if (activity == null)
            return;
        if (activity.isFinishing())
            return;

        c.show();
    }

    public void dismiss() {
        if (c.isShowing())
            c.dismiss();
    }

    public int getDialogType() {
        return dialogType;
    }

    public void setDialogType(int dialogType) {
        this.dialogType = dialogType;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public View.OnClickListener getEvetOnClickListener() {
        return evetOnClickListener;
    }

    public void setEvetOnClickListener(View.OnClickListener evetOnClickListener) {
        this.evetOnClickListener = evetOnClickListener;
    }

    public View.OnClickListener getHayirOnClickListener() {
        return hayirOnClickListener;
    }

    public void setHayirOnClickListener(
            View.OnClickListener hayirOnClickListener) {
        this.hayirOnClickListener = hayirOnClickListener;
    }

    public View.OnClickListener getTamamOnClickListener() {
        return tamamOnClickListener;
    }

    public void setTamamOnClickListener(
            View.OnClickListener tamamOnClickListener) {
        this.tamamOnClickListener = tamamOnClickListener;
    }

    public String getEvetButtonText() {
        return evetButtonText;
    }

    public void setEvetButtonText(String evetButtonText) {
        this.evetButtonText = evetButtonText;
    }

    public String getHayirButtonText() {
        return hayirButtonText;
    }

    public void setHayirButtonText(String hayirButtonText) {
        this.hayirButtonText = hayirButtonText;
    }

    public String getTamamButtonText() {
        return tamamButtonText;
    }

    public void setTamamButtonText(String tamamButtonText) {
        this.tamamButtonText = tamamButtonText;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMessageResourceId() {
        return messageResourceId;
    }

    public void setMessageResourceId(int messageResourceId) {
        this.messageResourceId = messageResourceId;
    }

    public int getButtonNumber() {
        return buttonNumber;
    }

    public void setButtonNumber(int buttonNumber) {
        this.buttonNumber = buttonNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public OnDismissListener getOnDismissListener() {
        return onDismissListener;
    }

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }
}
