package com.opusgo.vapiano.util;

import android.content.Context;
import android.content.SharedPreferences;


public class SharedPreferencesUtil {
	private static final String STORE_NAME = "VAPIONA_PREFERENCES";
	public static final String KEY_FIRST_RUN = "KEY_FIRST_RUN";
	public static final String OFFLINE_CATEGORY = "OFFLINE_CATEGORY";
	public static final String LanguageID = "LanguageID";
	public static final String CompanyID = "CompanyID";
	public static final String WorkStationID = "WorkStationID";
	public static final String CurrencyID = "CurrencyID";
	public static final String MachineNo = "MachineNo";
	public static final String TimerTimeout = "TimerTimeout";



	public static String getString(Context context, String key) {
		if(context == null)
			return "";
		SharedPreferences settings = context.getSharedPreferences(STORE_NAME,
				Context.MODE_PRIVATE);
		return settings.getString(key, "");
	}

	public static void setString(Context context, String key, String value) {
		if(context == null)
			return;
		if(value == null)
			value = "";
		SharedPreferences settings = context.getSharedPreferences(STORE_NAME,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static boolean isFirstRun(Context context) {
		String val = getString(context, KEY_FIRST_RUN);
		if (Validations.isEmpty(val))
			return true;
		else
			return Boolean.parseBoolean(val);
	}



	public static void setFirstRun(Context context) {
		setString(context, KEY_FIRST_RUN, "false");
	}



	public static boolean isCategoryOffline(Context context) {
		String val = getString(context, OFFLINE_CATEGORY);
		if (Validations.isEmpty(val))
			return true;
		else
			return Boolean.parseBoolean(val);
	}



	public static void setCategoryOffline(Context context) {
		setString(context, OFFLINE_CATEGORY, "false");
	}

	public static void setLanguageID(Context context,String mLanguageID) {
		setString(context, LanguageID, mLanguageID);
	}

	public static String getLanguageID(Context context) {
		String val = getString(context, LanguageID);
		if (Validations.isEmpty(val))
			return "";
		else
			return val;
	}

	public static void setCompanyID(Context context,String mCompanyID) {
		setString(context, CompanyID, mCompanyID);
	}

	public static String getCompanyID(Context context) {
		String val = getString(context, CompanyID);
		if (Validations.isEmpty(val))
			return "";
		else
			return val;
	}

	public static void setWorkStationID(Context context,String mWorkStationID) {
		setString(context, WorkStationID, mWorkStationID);
	}

	public static String getWorkStationID(Context context) {
		String val = getString(context, WorkStationID);
		if (Validations.isEmpty(val))
			return "";
		else
			return val;
	}

	public static void setCurrencyID(Context context,String mCurrencyID) {
		setString(context, CurrencyID, mCurrencyID);
	}

	public static String getCurrencyID(Context context) {
		String val = getString(context, CurrencyID);
		if (Validations.isEmpty(val))
			return "";
		else
			return val;
	}

	public static void setMachineNo(Context context,String mMachineNo) {
		setString(context, MachineNo, mMachineNo);
	}

	public static String getMachineNo(Context context) {
		String val = getString(context, MachineNo);
		if (Validations.isEmpty(val))
			return "";
		else
			return val;
	}

	public static void setTimerTimeout(Context context,String mTimerTimeout) {
		setString(context, TimerTimeout, mTimerTimeout);
	}

	public static String getTimerTimeout(Context context) {
		String val = getString(context, TimerTimeout);
		if (Validations.isEmpty(val))
			return "";
		else
			return val;
	}

}