package com.opusgo.vapiano.util;


import android.os.Environment;
import org.json.JSONArray;

import org.json.JSONObject;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * Created by hakankanmaz on 14.02.2017.
 */

public class AppUtil {

    public static void saveJSONArray(String path,JSONArray jsonArray) {

        try {
            // Writing to a file
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ "/Vapiano/"+path+".json");
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(jsonArray.toString());
            fileWriter.flush();
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveJSONObject(String path,JSONObject jsonObject) {

        try {
            // Writing to a file
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ "/Vapiano/"+path+".json");
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(jsonObject.toString());
            fileWriter.flush();
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
