package com.opusgo.vapiano.util;

import android.app.Activity;
import android.view.View;

public class DialogUtil {
	
	public static void showErrorDialog(final Activity activity, final String message)
	{
		if (activity == null)
			return;
		
		CustomDialog c = new CustomDialog(CustomDialog.DIALOG_ERROR, 1, activity, message);
		c.init();
		c.show();
	}

	public static void showErrorDialog(final Activity activity, final String message, View.OnClickListener onClickListener)
	{
		if (activity == null)
			return;

		CustomDialog c = new CustomDialog(CustomDialog.DIALOG_ERROR, 1, activity, message);
		c.setTamamOnClickListener(onClickListener);
		c.init();
		c.show();
	}
	
	public static void showSuccessDialog(final Activity activity, final String message)
	{
		if (activity == null)
			return;
		
		CustomDialog c = new CustomDialog(CustomDialog.DIALOG_POSITIVE, 1, activity, message);
		c.init();
		c.show();
	}

	public static void showSuccessDialog(final Activity activity, final String message, View.OnClickListener onClickListener)
	{
		if (activity == null)
			return;

		CustomDialog c = new CustomDialog(CustomDialog.DIALOG_POSITIVE, 1, activity, message);
		c.setTamamOnClickListener(onClickListener);
		c.init();
		c.show();
	}
	
	public static void showInfoDialog(final Activity activity, final String message)
	{
		if (activity == null)
			return;
		
		CustomDialog c = new CustomDialog(CustomDialog.DIALOG_INFO, 1, activity, message);
		c.init();
		c.show();
	}
	
	public static void showWarningDialog(final Activity activity, final String message)
	{
		if (activity == null)
			return;
		
		CustomDialog c = new CustomDialog(CustomDialog.DIALOG_WARNING, 1, activity, message);
		c.init();
		c.show();
	}

	public static void showInternetWarningDialog(final Activity activity, final String message)
	{
		if (activity == null)
			return;

		CustomDialog c = new CustomDialog(CustomDialog.DIALOG_WARNING, 1, activity, message);
		c.init();
		c.show();
	}
}
