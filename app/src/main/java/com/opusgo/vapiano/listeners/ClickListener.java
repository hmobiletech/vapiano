package com.opusgo.vapiano.listeners;

import android.view.View;

/**
 * Created by hakankanmaz on 5.02.2017.
 */

public interface ClickListener {
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}