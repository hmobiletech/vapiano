package com.opusgo.vapiano.listeners;

/**
 * Created by hakankanmaz on 7.02.2017.
 */

public interface ProductListener<T> {
    void onSuccess(T object);
    void onError(String message);
}
