package com.opusgo.vapiano.listeners;

/**
 * Created by hakankanmaz on 24.02.2017.
 */

public interface  RemovePriceListener {
     void onRemove(double price);
}
