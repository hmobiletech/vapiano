package com.opusgo.vapiano.listeners;

/**
 * Created by redugsi on 20/02/17.
 */

public interface ResponseListener<T> {
    void onSuccess(T object);
    void onError(String message);
}
