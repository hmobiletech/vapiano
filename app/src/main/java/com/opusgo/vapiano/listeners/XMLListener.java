package com.opusgo.vapiano.listeners;

/**
 * Created by hakankanmaz on 7.02.2017.
 */

public interface XMLListener{
    void onSuccess();
    void onError(String message);
}
