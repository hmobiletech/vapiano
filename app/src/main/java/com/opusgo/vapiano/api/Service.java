package com.opusgo.vapiano.api;

import android.content.Context;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.opusgo.vapiano.AppController;
import com.opusgo.vapiano.listeners.CategoryListener;
import com.opusgo.vapiano.listeners.LoginListener;
import com.opusgo.vapiano.listeners.ProductListener;
import com.opusgo.vapiano.listeners.ResponseListener;
import com.opusgo.vapiano.models.AuthorizedMembersRequest;
import com.opusgo.vapiano.models.AuthorizedMembersResponseList;
import com.opusgo.vapiano.models.CategoryListModel;
import com.opusgo.vapiano.models.OrderModel;
import com.opusgo.vapiano.models.OrderProductListModel;
import com.opusgo.vapiano.models.ProductClassListModel;
import com.opusgo.vapiano.models.ProductListModel;
import com.opusgo.vapiano.util.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hakankanmaz on 7.02.2017.
 */

public class Service {


    public static void getAllClasses(final ResponseListener<ProductClassListModel> responseListener) {
        final String url = ApiUtil.BASE_URL + "product/classes/all";

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                ProductClassListModel model = new Gson().fromJson(response.toString(), ProductClassListModel.class);
                Log.d("CACHE DATA Class", "Response");

                responseListener.onSuccess(model);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("CACHE DATA Class", "Error");

                responseListener.onError(error.getMessage());
            }
        }) {

           /* @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data);
                    JSONArray jsonArray = new JSONArray(jsonString);

                    // force response to be cached
                    Map<String, String> headers = response.headers;
                    long cacheExpiration = 20 *24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    Cache.Entry entry = new Cache.Entry();
                    entry.data = response.data;
                    entry.etag = headers.get("ETag");
                    entry.ttl = now + cacheExpiration;
                    entry.serverDate = HttpHeaderParser.parseDateAsEpoch(headers.get("Date"));
                    entry.responseHeaders = headers;

                    return Response.success(jsonArray, entry);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return Response.error(new VolleyError("Error parsing network response"));
                }
            }*/


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
                headers.put(Constants.ACCEPT, Constants.APPLICATION_JSON);
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.TIMEOUT, 1, 1));
        request.setShouldCache(true);
        AppController.getInstance().addToRequestQueue(request, url);


    }

    public static void getCategories(final Context context, final CategoryListener<CategoryListModel> responseListener) {

        Log.d("", "VRequests.getCategories");
        String url = ApiUtil.BASE_URL + "product/categories/all";


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("Volley", "Response: " + response.toString());

                CategoryListModel result = new Gson().fromJson(response.toString(), CategoryListModel.class);

                responseListener.onSuccess(result);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                responseListener.onError(error.getMessage());
            }
        }) {

           /* @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data);
                    JSONArray jsonArray = new JSONArray(jsonString);

                    // force response to be cached
                    Map<String, String> headers = response.headers;
                    long cacheExpiration = 20 *24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    Cache.Entry entry = new Cache.Entry();
                    entry.data = response.data;
                    entry.etag = headers.get("ETag");
                    entry.ttl = now + cacheExpiration;
                    entry.serverDate = HttpHeaderParser.parseDateAsEpoch(headers.get("Date"));
                    entry.responseHeaders = headers;

                    return Response.success(jsonArray, entry);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return Response.error(new VolleyError("Error parsing network response"));
                }
            }
*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
                headers.put(Constants.ACCEPT, Constants.APPLICATION_JSON);
                return headers;
            }
        };

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.TIMEOUT, 1, 1));
        jsonArrayRequest.setShouldCache(true);
        AppController.getInstance().addToRequestQueue(jsonArrayRequest, url);

    }

    public static void getProductByCategory(int categoryID, final Context context, final ProductListener<ProductListModel> responseListener) {

        Log.d("", "VRequests.getProductByCategory");


        String url = ApiUtil.BASE_URL + "product/categories/" + categoryID;


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("Volley", "Response: " + response.toString());

                ProductListModel result = new Gson().fromJson(response.toString(), ProductListModel.class);


                responseListener.onSuccess(result);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                responseListener.onError(error.getMessage());
            }
        }) {


            /*@Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data);
                    JSONArray jsonArray = new JSONArray(jsonString);

                    // force response to be cached
                    Map<String, String> headers = response.headers;
                    long cacheExpiration = 20 *24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    Cache.Entry entry = new Cache.Entry();
                    entry.data = response.data;
                    entry.etag = headers.get("ETag");
                    entry.ttl = now + cacheExpiration;
                    entry.serverDate = HttpHeaderParser.parseDateAsEpoch(headers.get("Date"));
                    entry.responseHeaders = headers;

                    return Response.success(jsonArray, entry);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return Response.error(new VolleyError("Error parsing network response"));
                }
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
                headers.put(Constants.ACCEPT, Constants.APPLICATION_JSON);
                return headers;
            }
        };

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.TIMEOUT, 1, 1));
        jsonArrayRequest.setShouldCache(true);

        AppController.getInstance().addToRequestQueue(jsonArrayRequest, url);
    }

    public static void getAuthorizedMembers(final Context context, String deviceCode, long workStation, final LoginListener responseListener) {

        Log.d("", "VRequests.Login");


        String url = "http://vapapi.opusgo.com/api/member/GetAuthorizedMembers";

        final AuthorizedMembersRequest request = new AuthorizedMembersRequest();
        request.setDeviceCode(deviceCode);
        request.setWorkStationID(workStation);

        String requestString = new Gson().toJson(request);

        Log.e("REQUEST", requestString);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST, url, requestString, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("Volley", "Response: " + response.toString());

                AuthorizedMembersResponseList result = new Gson().fromJson(response.toString(), AuthorizedMembersResponseList.class);

                if (result != null) {
                    responseListener.onSuccess(result);
                } else {
                    responseListener.onError("HATA");
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                responseListener.onError("HATA");


            }
        }) {
           /* @Override
            public String getBodyContentType() {

                return "application/x-www-form-urlencoded";
            }*/


            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put(Constants.ACCEPT, Constants.APPLICATION_JSON);
                headers.put(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
                return headers;
            }
        };

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.TIMEOUT, 1, 1));
        AppController.getInstance().addToRequestQueue(jsonArrayRequest, url);

    }



    //ORDER API

    public static void getOrdersByTerminalID(final Context context, long terminalId, final ResponseListener<OrderProductListModel> responseListener) {

        Log.d("", "VRequests.getOrdersByTerminalID");
        String url = ApiUtil.BASE_URL + "/order/" + terminalId;


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("Volley", "Response: " + response.toString());

                OrderProductListModel result = new Gson().fromJson(response.toString(), OrderProductListModel.class);

                responseListener.onSuccess(result);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                responseListener.onError(error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
                headers.put(Constants.ACCEPT, Constants.APPLICATION_JSON);
                return headers;
            }
        };

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.TIMEOUT, 1, 1));
        jsonArrayRequest.setShouldCache(true);
        AppController.getInstance().addToRequestQueue(jsonArrayRequest, url);

    }

    public static void order(Context context, OrderModel orderModel, final ResponseListener<Object> listener) {
        String url = ApiUtil.BASE_URL + "/order/postorder";
        String strRequest = new Gson().toJson(orderModel);
        Log.d("Request", strRequest);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, strRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Response", String.valueOf(response));
                listener.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Response", "HATA");
                listener.onError(error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
                headers.put(Constants.ACCEPT, Constants.APPLICATION_JSON);
                //headers.put("Authorization", getAuthentication("ceylan", "1234567"));

                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.TIMEOUT, 1, 1));
        AppController.getInstance().addToRequestQueue(request);
    }

    public static void getUnpayedOrders(String cardNo, final ResponseListener<OrderProductListModel> responseListener){
        Log.d("", "VRequests.getOrdersByTerminalID");
        String url = ApiUtil.BASE_URL + "/order/GetCustomerOrders/" + cardNo;


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("Volley", "Response: " + response.toString());

                OrderProductListModel result = new Gson().fromJson(response.toString(), OrderProductListModel.class);

                responseListener.onSuccess(result);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                responseListener.onError(error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
                headers.put(Constants.ACCEPT, Constants.APPLICATION_JSON);
                return headers;
            }
        };

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.TIMEOUT, 1, 1));
        jsonArrayRequest.setShouldCache(true);
        AppController.getInstance().addToRequestQueue(jsonArrayRequest, url);
    }

   /* private static String getAuthentication(String username, String password) {
        String credentials = String.format("%s:%s", username, password);
        String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT);
        return auth;
    }*/




   /* public static void getAllClassesFromCache(ResponseListener<ProductClassListModel> responseListener){
        final String url = ApiUtil.BASE_URL + "classes/all";


        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(url);
        if (entry != null) {

            //Cache data available.
            try {
                String data = new String(entry.data, "UTF-8");
                Log.d("CACHE DATA", data);

                JSONArray response = new JSONArray(data);
                ProductClassListModel result = new Gson().fromJson(response.toString(), ProductClassListModel.class);

                responseListener.onSuccess(result);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            // Cache data not exist.
            getAllClasses(responseListener);
        }
    }
*/
   /* public static void getCategoriesFromCache(final Context context, final CategoryListener<CategoryListModel> responseListener) {

        String url = ApiUtil.BASE_URL + "categories/all";


        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(url);
        if (entry != null) {

            //Cache data available.
            try {
                String data = new String(entry.data, "UTF-8");
                Log.d("CACHE DATA", data);

                JSONArray response = new JSONArray(data);
                CategoryListModel result = new Gson().fromJson(response.toString(), CategoryListModel.class);

                responseListener.onSuccess(result);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            // Cache data not exist.
            getCategories(context, responseListener);
        }
    }*/



   /* public static void getProductByCategoryFromCache(int categoryID,final Context context, final ProductListener<ProductListModel> responseListener) {

        String url = ApiUtil.BASE_URL + "categories/" + categoryID;

        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(url);
        if (entry != null) {

            //Cache data available.
            try {
                String data = new String(entry.data, "UTF-8");
                Log.d("CACHE DATA", data);

                JSONArray response = new JSONArray(data);
                ProductListModel result = new Gson().fromJson(response.toString(), ProductListModel.class);

                responseListener.onSuccess(result);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            // Cache data not exist.
            getProductByCategory(categoryID,context, responseListener);
        }
    }*/


     /* public static void auth(final String userId, final String password, final Context context, final LoginListener responseListener) {

        Log.d("","VRequests.Login");

        final LoginRequest request = new LoginRequest();
       *//* DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setDeviceType(Constants.ANDROID);
        deviceInfo.setUuid(getAdId(context));
        deviceInfo.setName(Build.MODEL);
        deviceInfo.setNewDevice(SharedPreferencesUtil.getNewDevice(context));*//*
        request.setUserid(userId);
        request.setPassword(password);

        String url = ApiUtil.BASE_URL + "login";

        String requestString = new Gson().toJson(request);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, requestString, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Volley", "Response: " + response.toString());

                 LoginResponse result = new Gson().fromJson(response.toString(), LoginResponse.class);

                if(result.isSucced()){
                    responseListener.onSuccess(result);
                } else {
                    responseListener.onError(result.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put(Constants.CONTENT_TYPE,Constants.APPLICATION_JSON);
                    headers.put(Constants.ACCEPT,Constants.APPLICATION_JSON);
                    return headers;
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.TIMEOUT, 1, 1));
        jsonObjectRequest.setShouldCache(false);
        if (context != null) {
            VolleyClient.getInstance(context.getApplicationContext()).addToRequestQueue(jsonObjectRequest);
        }
    }
*/

        /* public static void getMember(final Context context, int memberId, final ResponseListener<PersonelModel> responseListener){
        String url = ApiUtil.BASE_URL + "/member/" + memberId;

        JsonObjectRequest request = new JsonObjectRequest(url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                PersonelModel result = new Gson().fromJson(response.toString(), PersonelModel.class);
                Log.d("FAFAF",result.getFullName()+"");

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("FAFAF",error+"");
            }
        });
//        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d("FAFAF",response);
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("FAFAF",error+"");
//            }
//        });

        AppController.getInstance().addToRequestQueue(request,url);

    }*/
}
